variables:
    EOS_PATH: "/eos/project/c/corryvreckan/www/"
    DOCKER_FILE: etc/docker/Dockerfile

stages:
    - compilation
    - testing
    - formatting
    - documentation
    - packaging
    - deployment

#######################
# Compilation targets #
#######################


# Hidden key to define the default compile job:
.compile:
    stage: compilation
    needs: []
    tags:
        - docker
    script:
        - mkdir build
        - cd build
        - cmake -GNinja -DCMAKE_CXX_FLAGS="-Werror" -DCMAKE_BUILD_TYPE=RELEASE -DROOT_DIR=$ROOTSYS -DEigen3_DIR=$Eigen3_DIR ..
        - ninja -k0
        - ninja install
    artifacts:
        paths:
            - build
            - bin
            - lib
        expire_in: 24 hour

cmp:slc6-gcc:
    extends: .compile
    image: gitlab-registry.cern.ch/sft/docker/slc6:latest
    before_script:
        - export COMPILER_TYPE="gcc"
        - source .gitlab/ci/init_x86_64.sh
        - source .gitlab/ci/load_deps.sh

cmp:cc7-gcc:
    extends: .compile
    image: gitlab-registry.cern.ch/sft/docker/centos7:latest
    before_script:
        - export COMPILER_TYPE="gcc"
        - source .gitlab/ci/init_x86_64.sh
        - source .gitlab/ci/load_deps.sh

cmp:cc7-docker:
    extends: .compile
    image:
        name: gitlab-registry.cern.ch/corryvreckan/corryvreckan/corryvreckan-deps
        entrypoint: [""]
    before_script:
        - source scl_source enable devtoolset-8 || echo " "
    script:
        - mkdir build
        - cd build
        - cmake3 -DCMAKE_CXX_FLAGS="-Werror" -DBUILD_EventLoaderEUDAQ2=ON -DBUILD_EventDefinitionM26=ON -DCMAKE_BUILD_TYPE=RELEASE -DROOT_DIR=$ROOTSYS -DEigen3_DIR=$Eigen3_DIR -Deudaq_DIR=/opt/eudaq2/cmake ..
        - make
        - make install

cmp:cc7-llvm:
    extends: .compile
    image: gitlab-registry.cern.ch/sft/docker/centos7:latest
    before_script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab/ci/init_x86_64.sh

cmp:mac1015-clang:
    extends: .compile
    tags:
        - mac
    before_script:
        - source .gitlab/ci/init_x86_64.sh
        - source .gitlab/ci/load_deps.sh
    script:
        - mkdir build
        - cd build
        - cmake -GNinja -DCMAKE_CXX_FLAGS="-Werror" -DCMAKE_BUILD_TYPE=RELEASE -DROOT_DIR=$ROOTSYS -DCMAKE_USE_RELATIVE_PATHS=TRUE -DEigen3_DIR=$Eigen3_DIR ..
        - ninja -k0
        - ninja install


##############
# Unit tests #
##############

.test:
    stage: testing
    tags:
        - docker
    needs:
        - job: cmp:cc7-docker
          artifacts: true
    image:
        name: gitlab-registry.cern.ch/corryvreckan/corryvreckan/corryvreckan-deps
        entrypoint: [""]
    before_script:
        - source scl_source enable devtoolset-8 || echo " "
    after_script:
        - ./.gitlab/ci/transform_ctest_junit.py build/Testing/`head -n 1 build/Testing/TAG`/Test.xml .gitlab/ci/ctest-to-junit.xsl corry-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}.xml
    artifacts:
        when: always
        expire_in: 1 day
        name: "corry-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}"
        paths:
            - corry-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}.xml
        reports:
            junit: corry-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}.xml

tst:tracking:
    extends: .test
    script:
        - cd build/
        - mkdir -p ../testing/data
        - ctest -R test_tracking --no-compress-output --test-action Test -j1

tst:align:
    extends: .test
    script:
        - cd build/
        - mkdir -p ../testing/data
        - ctest -R test_align --no-compress-output --test-action Test -j1

tst:io:
    extends: .test
    script:
        - cd build/
        - mkdir -p ../testing/data
        - ctest -R test_io --no-compress-output --test-action Test -j1

tst:sim:
    extends: .test
    script:
        - cd build/
        - mkdir -p ../testing/data
        - ctest -R test_sim --no-compress-output --test-action Test -j1


############################
# Format and Lint Checking #
############################

# Hidden key to define the basis for linting and formatting:
.format:
    stage: formatting
    tags:
        - docker
    image: gitlab-registry.cern.ch/sft/docker/centos7:latest
    before_script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab/ci/init_x86_64.sh

fmt:centos7-llvm-format:
    extends: .format
    needs: []
    dependencies: []
    script:
        - mkdir -p build
        - cd build/
        - cmake -GNinja -DCMAKE_CXX_FLAGS="-Werror" -DCMAKE_BUILD_TYPE=RELEASE -DROOT_DIR=$ROOTSYS -DEigen3_DIR=$Eigen3_DIR ..
        - ninja check-format

fmt:cc7-llvm-lint:
    extends: .format
    needs: []
    dependencies: []
    script:
        - mkdir -p build
        - cd build/
        - cmake -GNinja -DCMAKE_CXX_FLAGS="-Werror" -DCMAKE_BUILD_TYPE=RELEASE -DROOT_DIR=$ROOTSYS -DEigen3_DIR=$Eigen3_DIR ..
        - ninja check-lint

fmt:codespell:
    extends: .format
    needs: []
    dependencies: []
    before_script:
        - export COMPILER_TYPE="gcc"
        - source .gitlab/ci/init_x86_64.sh
        - export PATH=~/.local/bin:$PATH
        - pip install --trusted-host=pypi.org --user codespell
    script:
        - codespell --ignore-words .gitlab/ci/codespell_ignored_words.txt --quiet-level 2 --skip ".git,.gitlab,cmake,3rdparty,*.svg"


#############################
# Documentation Compilation #
#############################

.doc:
    stage: documentation
    needs: []
    dependencies: []
    tags:
        - docker
    artifacts:
        paths:
            - public
        expire_in: 24 hour

# Compile Doxygen reference
cmp:doxygen:
    extends: .doc
    image: gitlab-registry.cern.ch/sft/docker/centos7:latest
    before_script:
        - source .gitlab/ci/init_x86_64.sh
        - source .gitlab/ci/load_deps.sh
    script:
        - mkdir -p public/usermanual
        - mkdir build
        - cd build
        - cmake -DBUILD_DOCS_ONLY=ON ..
        - make Corryvreckan-reference
        - mv reference/html ../public/reference

# Compile LaTeX user manual:
cmp:usermanual:
    extends: .doc
    image: gitlab-registry.cern.ch/clicdp/publications/templates/custom_ci_worker:fedora-latex-latest
    script:
        - mkdir -p public/usermanual
        - mkdir build
        - cd build
        - cmake -DBUILD_DOCS_ONLY=ON ..
        - make pdf
        - mv usermanual/corryvreckan-manual.pdf ../public/usermanual


################################
# Packaging of Binary Tarballs #
################################


.pack:
    stage: packaging
    tags:
        - docker
    only:
        - tags@corryvreckan/corryvreckan
        - schedules@corryvreckan/corryvreckan
    before_script:
        - export COMPILER_TYPE="gcc"
        - source .gitlab/ci/init_x86_64.sh
        - source .gitlab/ci/load_deps.sh
    script:
        - mkdir -p public/releases
        - cd build
        - cmake -GNinja -DCMAKE_SKIP_RPATH=ON -DCMAKE_INSTALL_PREFIX=/tmp ..
        - ninja package
        - mv *.tar.gz ../public/releases
    artifacts:
        paths:
            - public
        expire_in: 24 hour

pkg:slc6-gcc:
    extends: .pack
    image: gitlab-registry.cern.ch/sft/docker/slc6:latest
    needs:
        - job: cmp:slc6-gcc
          artifacts: true

pkg:cc7-gcc:
    extends: .pack
    image: gitlab-registry.cern.ch/sft/docker/centos7:latest
    needs:
        - job: cmp:cc7-gcc
          artifacts: true


########################
# Automatic Deployment #
########################

# Automatically deploy documentation to the website
# Deployment job only executed for new tag pushs, not for every commit.
deploy-documentation:
    stage: deployment
    tags:
      - docker
    variables:
        GIT_STRATEGY: none
    # Only run for new tags:
    only:
        - tags@corryvreckan/corryvreckan
    dependencies:
        - cmp:usermanual
        - cmp:doxygen
    # Docker image with tools to deploy to EOS
    image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
    script:
        - deploy-eos
    # do not run any globally defined before_script or after_script for this step
    before_script: []
    after_script: []

deploy-eos:
    stage: deployment
    tags:
      - docker
    variables:
        GIT_STRATEGY: none
    # Only run for new tags:
    only:
        - tags@corryvreckan/corryvreckan
        - schedules@corryvreckan/corryvreckan
    dependencies:
        - pkg:cc7-gcc
        - pkg:slc6-gcc
    # Docker image with tools to deploy to EOS
    image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
    script:
        - deploy-eos
    # do not run any globally defined before_script or after_script for this step
    before_script: []
    after_script: []

deploy-cvmfs:
    stage: deployment
    dependencies:
        - pkg:cc7-gcc
        - pkg:slc6-gcc
    tags:
        - cvmfs-deploy
    only:
        - tags@corryvreckan/corryvreckan
        - schedules@corryvreckan/corryvreckan
    script:
        - ./.gitlab/ci/download_artifacts.py $API_TOKEN $CI_PROJECT_ID $CI_PIPELINE_ID
        - export RUNNER_LOCATION=$(pwd)
        - if [ -z ${CI_COMMIT_TAG} ]; then export BUILD_PATH='latest'; else export BUILD_PATH=${CI_COMMIT_TAG}; fi
        - sudo -u cvclicdp -i $RUNNER_LOCATION/.gitlab/ci/gitlab_deploy.sh $RUNNER_LOCATION $BUILD_PATH
        - rm -f corryvreckan-*.tar.gz
    retry: 1

deploy-docker-latest:
    stage: deployment
    tags:
        - docker-image-build
    dependencies: []
    only:
        - schedules@corryvreckan/corryvreckan
    script:
        - "echo" # unused but this line is required by GitLab CI
    variables:
        TO: gitlab-registry.cern.ch/corryvreckan/corryvreckan

deploy-docker-tag:
    stage: deployment
    tags:
        - docker-image-build
    dependencies: []
    only:
        - tags@corryvreckan/corryvreckan
    script:
        - "echo" # unused but this line is required by GitLab CI
    variables:
        TO: gitlab-registry.cern.ch/corryvreckan/corryvreckan:${CI_COMMIT_TAG}
